package ch.festigeek.android.festi_app.interfaces;

/**
 * Created by Pierre-Alain Curty on 20.05.2018.
 */
public interface FGFragment {
    public Integer getFragmentTitle();
}
