package ch.festigeek.android.festi_app.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import ch.festigeek.android.festi_app.R;
import ch.festigeek.android.festi_app.entities.Product;
import ch.festigeek.android.festi_app.interfaces.IKeys;
import ch.festigeek.android.festi_app.interfaces.IURL;

public class ListProductAdapter extends ArrayAdapter<Product> implements IKeys, IURL {
    private List<Product> mList;

    public ListProductAdapter(Context context, int resource, List<Product> list) {
        super(context, resource, list);
        mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Product getItem(int position) {
        return mList.get(position);
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_product, parent, false);
        }

        final Product product = mList.get(position);

        if(product.getProduct_type_id().equals(1)) {
            convertView.setVisibility(View.GONE);
            return convertView;
        }

        AppCompatTextView name = (AppCompatTextView) convertView.findViewById(R.id.product_text);
        final AppCompatButton consume = (AppCompatButton) convertView.findViewById(R.id.button_consume);
        final AppCompatButton cancel = (AppCompatButton) convertView.findViewById(R.id.button_cancel);

        String str = product.getName() + " (" + product.getConsume() + "/" + product.getAmount() + ")";
        name.setText(str);
        consume.setEnabled(!product.getConsume().equals(product.getAmount()));
        cancel.setEnabled(product.getConsume() > 0);

        consume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String url = BASE_URL + ORDERS + order.getOrderId() + PRODUCTS + order.getProductId();
//                Log.e(LOG_NAME, "value " + order.isUsed());
//                new RequestPATCH(new ICallback<String>() {
//                    @Override
//                    public void success(String result) {
//                        Log.e(LOG_NAME, result);
//                        order.use(order.isUsed() + 1);
//                        mActivity.init();
//                    }
//
//                    @Override
//                    public void failure(Exception ex) {
//                        Log.e(LOG_NAME, ex.getMessage());
//                    }
//                }, Utilities.getFromSharedPreferences(getContext(), "token"), url, "consume", order.isUsed() + 1).execute();
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String url = BASE_URL + ORDERS + order.getOrderId() + PRODUCTS + order.getProductId();
//                Log.e(LOG_NAME, "value " + order.isUsed());
//                new RequestPATCH(new ICallback<String>() {
//                    @Override
//                    public void success(String result) {
//                        Log.e(LOG_NAME, result);
//                        order.use(order.isUsed() - 1);
//                        mActivity.init();
//                    }
//
//                    @Override
//                    public void failure(Exception ex) {
//                        Log.e(LOG_NAME, ex.getMessage());
//                    }
//                }, Utilities.getFromSharedPreferences(getContext(), "token"), url, "consume", order.isUsed() - 1).execute();
            }
        });

        return convertView;
    }
}
