package ch.festigeek.android.festi_app.entities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ch.festigeek.android.festi_app.interfaces.IKeys;

public class Team implements IKeys {

    private final Integer teamId;
    private final String name;
    private final Integer numberOfMember;
    private final Integer gameId;
    private final String data;

    public Team(JSONObject team) throws JSONException {
        teamId = team.getInt(KEY_ID);
        name = team.getString(KEY_NAME);
        gameId = team.getInt(KEY_GAME_ID);
        JSONArray memberList = team.getJSONArray(KEY_USERS);
        numberOfMember = memberList.length();
        data = team.toString();
    }

    public Integer getTeamId() {
        return teamId;
    }

    public String getName() {
        return name;
    }

    public Integer getNumberOfMember() {
        return numberOfMember;
    }

    public Integer getGameId() {
        return gameId;
    }

    @Override
    public String toString() {
        return super.toString() + " => " + data;
    }
}
