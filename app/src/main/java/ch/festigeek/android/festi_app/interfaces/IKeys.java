package ch.festigeek.android.festi_app.interfaces;

public interface IKeys {
    // ALL
    String APP_NAME = "FestiApp";

    String KEY_ID = "id";
    String KEY_PIVOT = "pivot";
    String KEY_USERS = "users";
    String KEY_USER = "user";
    String KEY_PRODUCTS = "products";
    String KEY_ORDER_ID = "order_id";
    String KEY_NAME = "name";
    String KEY_CREATED = "created_at";
    String KEY_UPDATED = "updated_at";

    // USER
    String KEY_EMAIL = "email";
    String KEY_USERNAME = "username";
    String KEY_BIRTHDATE = "birthdate";
    String KEY_GENDER = "gender";
    String KEY_FIRSTNAME = "firstname";
    String KEY_LASTNAME = "lastname";
    String KEY_STREET = "street";
    String KEY_NPA = "npa";
    String KEY_CITY = "city";
    String KEY_LOL_ACCOUNT = "lol_account";
    String KEY_STEAM = "steamID64";
    String KEY_BATTLE_TAG = "battleTag";

    // ORDER
    String KEY_USER_ID = "user_id";
    String KEY_STATE = "state";
    String KEY_CODE_LAN = "code_lan";
    String KEY_TEAM = "team";
    String KEY_PAYMENT_TYPE = "payment_type";
    String KEY_PAYMENT_TYPE_NAME = "name";
    String KEY_TOTAL = "total";

    // TEAM
    String KEY_GAME_ID = "game";

    // PRODUCT
    String KEY_PRODUCT_TYPE_ID = "product_type_id";
    String KEY_PRICE = "price";
    String KEY_AMOUNT = "amount";
    String KEY_CONSUME = "consume";
}
