package ch.festigeek.android.festi_app;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.single.PermissionListener;

import butterknife.BindView;
import butterknife.ButterKnife;

import ca.mimic.oauth2library.OAuthResponse;
import ca.mimic.oauth2library.OAuthResponseCallback;
import ch.festigeek.android.festi_app.interfaces.IKeys;
import ch.festigeek.android.festi_app.interfaces.IURL;
import ch.festigeek.android.festi_app.utils.CommunicationManager;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements IKeys, IURL {

    private final Integer LOGIN_RESULT = 100;

    // Data
    private SharedPreferences sharedPref = null;

    // Communication
    CommunicationManager communicationManager = null;

    // UI references.
    @BindView(R.id.email)
    protected EditText mEmailView;
    @BindView(R.id.password)
    protected EditText mPasswordView;
    @BindView(R.id.login_progress)
    protected ProgressBar mProgressView;
    @BindView(R.id.email_sign_in_button)
    protected Button mEmailSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        sharedPref = getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);

        Dexter.withActivity(this)
            .withPermission(Manifest.permission.INTERNET)
            .withListener(new PermissionListener() {
                @Override
                public void onPermissionGranted(PermissionGrantedResponse response) {
                    // permission is granted, do things...
                }

                @Override
                public void onPermissionDenied(PermissionDeniedResponse response) {
                    // check for permanent denial of permission
                    if (response.isPermanentlyDenied())
                        showSettingsDialog();
                }

                @Override
                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                    token.continuePermissionRequest();
                }
            })
            .withErrorListener(new PermissionRequestErrorListener() {
                @Override
                public void onError(DexterError error) {
                    Toast.makeText(getApplicationContext(), "Error occurred with Dexter ! " + error.toString(), Toast.LENGTH_SHORT).show();
                }
            })
            .check();

        // Set up private variables.
        communicationManager = new CommunicationManager(this);

        // Set up the login form.
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                attemptLogin();
            }
        });

        // Check if an email as been saved
        mEmailView.setText(sharedPref.getString("stored_mail", ""));
    }

    private void resetPasswordWithError(String error){
        //mPasswordView.setText("");
        mPasswordView.getText().clear();
        mPasswordView.setError(error);
        mPasswordView.requestFocus();
    }

    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        final String email = mEmailView.getText().toString();
        final String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password) || !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }
        else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if(cancel)
            focusView.requestFocus();
        else {
            // Show a progress bar, to perform the user login attempt.
            mProgressView.setVisibility(View.VISIBLE);

            communicationManager.getClient(email, password).requestAccessToken(new OAuthResponseCallback() {
                @Override
                public void onResponse(final OAuthResponse response) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            if (response != null && response.isSuccessful()) {
                                SharedPreferences.Editor editor = sharedPref.edit();
                                String accessToken = response.getAccessToken();
                                String refreshToken = response.getRefreshToken();
                                Long expiresAt = response.getExpiresAt();

                                if (expiresAt != null) {
                                    editor.putLong(getString(R.string.sp_expires_at), expiresAt);
                                    editor.commit();
                                }

                                if (refreshToken != null) {
                                    editor.putString(getString(R.string.sp_refresh_token), refreshToken);
                                    editor.commit();
                                }

                                if (accessToken != null) {
                                    editor.putString(getString(R.string.sp_token), accessToken);
                                    editor.putString(getString(R.string.sp_mail), email);
                                    editor.commit();

                                    Intent data = new Intent();
                                    data.putExtra("login", "succeeded");
                                    setResult(LOGIN_RESULT, data);
                                    finish();
                                } else {
                                    resetPasswordWithError(getString(R.string.auth_failed));
                                }
                            }
                            else {
                                Integer res = (response != null && response.getCode() != null) ? response.getCode() : 0;
                                switch (res) {
                                    case 401:
                                        resetPasswordWithError(getString(R.string.auth_failed));
                                        break;
                                    default:
                                        resetPasswordWithError(getString(R.string.connexion_failed));
                                }
                            }

                            mProgressView.setVisibility(View.INVISIBLE);
                        }
                    });
                }
            });
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("Permissions nécessaires");
        builder.setMessage("FestiApp nécessite de pouvoir accéder à internet. Vous pouvez changer les permissions dans les préférences de l'application.");
        builder.setPositiveButton("Ouvrir préférences", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();

            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            startActivityForResult(intent, 101);
            }
        });
        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            Intent data = new Intent();
            data.putExtra("login", "failed");
            setResult(LOGIN_RESULT, data);
            finish();
        }

        return super.onKeyDown(keyCode, event);
    }

    private boolean isEmailValid(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 8;
    }
}

