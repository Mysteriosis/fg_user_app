package ch.festigeek.android.festi_app.vision.ocrreader.ui.vision;

import android.util.SparseArray;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.Line;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;

import java.util.List;

import ch.festigeek.android.festi_app.entities.OcrGraphic;

/**
 * A very simple Processor which gets detected TextBlocks and adds them to the overlay
 * as OcrGraphics.
 * TODO: Make this implement Detector.Processor<TextBlock> and add text to the GraphicOverlay
 */
public class OcrDetectorProcessor implements Detector.Processor<TextBlock> {

    private GraphicOverlay<OcrGraphic> mGraphicOverlay;

    public OcrDetectorProcessor(GraphicOverlay<OcrGraphic> ocrGraphicOverlay) {
        mGraphicOverlay = ocrGraphicOverlay;
    }

    @Override
    public void receiveDetections(Detector.Detections<TextBlock> detections) {
        mGraphicOverlay.clear();
        SparseArray<TextBlock> items = detections.getDetectedItems();
        for (int i = 0; i < items.size(); ++i) {
            TextBlock item = items.valueAt(i);
            if (item != null && item.getValue() != null) {
//                Log.d("Processor", "Text detected! " + item.getValue());
                List<? extends Text> textComponents = item.getComponents();
                for(Text line : textComponents) {
                    mGraphicOverlay.add(new OcrGraphic(mGraphicOverlay, (Line)line));
                }
            }
        }
    }

    @Override
    public void release() {
        mGraphicOverlay.clear();
    }
}
