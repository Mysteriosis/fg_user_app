package ch.festigeek.android.festi_app.entities;

import org.json.JSONException;
import org.json.JSONObject;

import ch.festigeek.android.festi_app.interfaces.IKeys;

public class Product implements IKeys {

    private final Integer productId;
    private final String name;
    private final Integer price;
    private final Integer product_type_id;
    private final Integer amount;
    private Integer consume;
    private final String data;

    public Product(JSONObject product) throws JSONException {
        productId = product.getInt(KEY_ID);
        name = product.getString(KEY_NAME);
        price = product.getInt(KEY_PRICE);
        product_type_id = product.getInt(KEY_PRODUCT_TYPE_ID);
        amount = product.getJSONObject(KEY_PIVOT).getInt(KEY_AMOUNT);
        consume = product.getJSONObject(KEY_PIVOT).getInt(KEY_CONSUME);
        data = product.toString();
    }

    public Integer getProductId() {
        return productId;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getProduct_type_id() {
        return product_type_id;
    }

    public Integer getAmount() {
        return amount;
    }

    public Integer getConsume() {
        return consume;
    }

    @Override
    public String toString() {
        return super.toString() + " => " + data;
    }

    // SPECIAL

    public Boolean isTournament() {
        return product_type_id == 1;
    }

    public void setConsume(Integer consume) {
        this.consume = consume;
    }
}
