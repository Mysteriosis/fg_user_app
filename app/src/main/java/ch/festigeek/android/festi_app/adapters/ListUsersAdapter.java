package ch.festigeek.android.festi_app.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import ch.festigeek.android.festi_app.MainActivity;
import ch.festigeek.android.festi_app.R;
import ch.festigeek.android.festi_app.entities.OrderUser;
import ch.festigeek.android.festi_app.fragments.OrderFragment;
import ch.festigeek.android.festi_app.interfaces.IKeys;

public class ListUsersAdapter extends ArrayAdapter<OrderUser> implements Filterable, IKeys {

    private List<OrderUser> mAllUsers;
    private List<OrderUser> mFilteredUsers;

    public ListUsersAdapter(Context context, int resource, List<OrderUser> allUsers) {
        super(context, resource, allUsers);
        mAllUsers = allUsers;
        mFilteredUsers = allUsers;
    }

    @Override
    public int getCount() {
        return mFilteredUsers.size();
    }

    @Override
    public OrderUser getItem(int position) {
        return mFilteredUsers.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_order, parent, false);
        }

        final OrderUser orderUser = getItem(position);
        AppCompatTextView username = (AppCompatTextView) convertView.findViewById(R.id.item_username);
        if (username != null && orderUser != null) {
            username.setText(orderUser.getUsername());
        }

        LinearLayoutCompat line = (LinearLayoutCompat) convertView.findViewById(R.id.item_line);
        line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(KEY_ORDER_ID, orderUser.getOrderId());
                ((MainActivity) getContext()).changeFragment(OrderFragment.class, bundle, true);
            }
        });

        AppCompatImageView icon = (AppCompatImageView) convertView.findViewById(R.id.order_icon);
        if (orderUser.isCheckedIn()) {
            icon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_error_outline_red_24dp));
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<OrderUser> filteredUsers;
                if (constraint != null && constraint.length() != 0) {
                    filteredUsers = new ArrayList<>();
                    for (OrderUser user : mAllUsers) {
                        if (user.contains(constraint.toString())) {
                            filteredUsers.add(user);
                        }
                    }
                } else {
                    filteredUsers = mAllUsers;
                }
                results.count = filteredUsers.size();
                results.values = filteredUsers;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mFilteredUsers = (List<OrderUser>) results.values;
                if (results.count == 0) {
                    notifyDataSetInvalidated();
                } else {
                    notifyDataSetChanged();
                }
            }
        };
    }
}
