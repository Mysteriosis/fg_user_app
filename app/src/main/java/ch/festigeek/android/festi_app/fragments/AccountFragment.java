package ch.festigeek.android.festi_app.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ch.festigeek.android.festi_app.R;
import ch.festigeek.android.festi_app.MainActivity;
import ch.festigeek.android.festi_app.interfaces.FGFragment;
import ch.festigeek.android.festi_app.interfaces.IKeys;
import ch.festigeek.android.festi_app.interfaces.IURL;
import ch.festigeek.android.festi_app.utils.CommunicationManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AccountFragment.OnFragmentInteractionListener} interface
 * to handle interaction events and the
 * {@link SwipeRefreshLayout.OnRefreshListener} interface
 * to handle swiping down events.
 * Use the {@link AccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountFragment extends Fragment implements FGFragment, IKeys, IURL, SwipeRefreshLayout.OnRefreshListener {

    private final String FRAGMENT_TAG = "ACCOUNT_FRAGMENT";
    private final String ACCOUNT_REQUEST = "ACCOUNT";

    private MainActivity activity;

    // Data
    private RequestQueue queue = null;
    private SharedPreferences sharedPref = null;

    private Unbinder unbinder;
    private OnFragmentInteractionListener mListener;
    @BindView(R.id.accountSwipeRefresh)
    protected SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.greeting)
    protected AppCompatTextView greeting_field;
    @BindView(R.id.mail)
    protected AppCompatTextView mail_field;
    @BindView(R.id.qrcode)
    protected AppCompatImageView qrcode_field;
    @BindView(R.id.username)
    protected AppCompatTextView username_field;
    @BindView(R.id.birthdate)
    protected AppCompatTextView birthdate_field;
    @BindView(R.id.steam_id)
    protected AppCompatTextView steamid_field;
    @BindView(R.id.battle_tag)
    protected AppCompatTextView battle_tag_field;
    @BindView(R.id.lolTag)
    protected AppCompatTextView lol_tag_field;


    public AccountFragment() {
        // Required empty public constructor
    }

    public static AccountFragment newInstance() {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(ch.festigeek.android.festi_app.R.layout.fragment_account, container, false);
        activity = (MainActivity) getActivity();

        unbinder = ButterKnife.bind(this, view);
        sharedPref = getActivity().getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);
        swipeRefreshLayout.setOnRefreshListener(this);

        HurlStack hurlStack = activity.communicationManager.createHurlStack(getActivity());
        queue = Volley.newRequestQueue(getContext(), hurlStack);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.actions_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void attemptGetUserInfo() {
        activity.showProgressBar();
        Map<String, String> jsonParams = new HashMap<>();

        Log.i("REQUEST", PROFILE);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, PROFILE, new JSONObject(jsonParams),
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    populateFields(response);
                    activity.hideProgressBar();
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CommunicationManager.errorMessages(error, getActivity());
                    activity.hideProgressBar();
                }
            }) { // needs headers for the request, override header function of this object (not the general class)
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", "Bearer " + sharedPref.getString(getString(R.string.sp_token), ""));
                return headers;
            }
        };

        jsObjRequest.setTag(ACCOUNT_REQUEST);
        queue.add(jsObjRequest);
    }

    private void populateFields(JSONObject data) {
        // TODO: Use User Model
        try{
            // set greeting depending of time of day
            Calendar instance = Calendar.getInstance();
            int hour = instance.get(Calendar.HOUR_OF_DAY);

            String greetingsText = (hour>17 || hour < 3) ? "Bonsoir " + data.getString("firstname") + " !" : "Bonjour " + data.getString("firstname") + " !";
            greeting_field.setText(greetingsText );

            mail_field.setText(data.getString("email"));
            username_field.setText(data.getString("username"));
            birthdate_field.setText(data.getString("birthdate"));

            String encodedImage = data.getString("QRCode");
            byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            qrcode_field.setImageBitmap(decodedByte);

            if(!data.getString("steamID64").equals("null")){
                steamid_field.setText(data.getString("steamID64"));
            }
            if(!data.getString("battleTag").equals("null")){
                battle_tag_field.setText(data.getString("battleTag"));
            }
            if(!data.getString("lol_account").equals("null")){
                lol_tag_field.setText(data.getString("lol_account"));
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Integer getFragmentTitle() {
        return R.string.fragment_account_title;
    }

    @Override
    public void onResume() {
        super.onResume();
        attemptGetUserInfo();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
        else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        attemptGetUserInfo();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        mListener = null;
        queue.cancelAll(ACCOUNT_REQUEST);
        unbinder.unbind();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
