package ch.festigeek.android.festi_app.utils;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Mysteriosis on 07.05.18.
 */

public class SSLExceptions {
    private static SSLExceptions instance = null;

    private TrustManager[] trustManagers;
    private HostnameVerifier hostnameVerifier;
    private KeyStore keyStore;

    private SSLExceptions() {
        try {
            keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null, null);
        }
        catch (Exception e) {
            Log.e("SSLException instantiation", e.getMessage());
        }
    }

    public static SSLExceptions getInstance() {
        if (instance == null)
            instance = getSync();
        return instance;
    }

    private static synchronized SSLExceptions getSync() {
        if(instance == null)
            instance = new SSLExceptions();
        return instance;
    }

    private HostnameVerifier getHostnameVerifier(final String name) {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                //return true; // verify always returns true, which could cause insecure network traffic due to trusting TLS/SSL server certificates for wrong hostnames
                HostnameVerifier hv = HttpsURLConnection.getDefaultHostnameVerifier();
                return hv.verify(name, session);
            }
        };
    }

    private TrustManager[] getWrappedTrustManagers(TrustManager[] trustManagers) {
        final X509TrustManager originalTrustManager = (X509TrustManager) trustManagers[0];
        return new TrustManager[] {
            new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return originalTrustManager.getAcceptedIssuers();
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    try {
                        if (certs != null && certs.length > 0) {
                            certs[0].checkValidity();
                        }
                        else {
                            originalTrustManager
                                    .checkClientTrusted(certs, authType);
                        }
                    }
                    catch (CertificateException e) {
                        Log.w("checkClientTrusted", e.toString());
                    }
                }

                public void checkServerTrusted(X509Certificate[] certs,
                                               String authType) {
                    try {
                        if (certs != null && certs.length > 0) {
                            certs[0].checkValidity();
                        } else {
                            originalTrustManager
                                    .checkServerTrusted(certs, authType);
                        }
                    } catch (CertificateException e) {
                        Log.w("checkServerTrusted", e.toString());
                    }
                }
            }
        };
    }

    public void addCertificate(InputStream certInputStream, String name) {
        try {
            // InputStream certInputStream = context.getAssets().open("festigeek.ch.pem");
            BufferedInputStream bis = new BufferedInputStream(certInputStream);
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");

            while (bis.available() > 0) {
                Certificate cert = certificateFactory.generateCertificate(bis);
                // keyStore.setCertificateEntry("www.festigeek.ch", cert);
                keyStore.setCertificateEntry(name, cert);
            }

            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(keyStore);

            trustManagers = getWrappedTrustManagers(trustManagerFactory.getTrustManagers());
            hostnameVerifier = getHostnameVerifier(name);
        }
        catch (Exception e) {
            Log.e("SSLException", e.getMessage());
        }
    }

    public SSLContext getSslContext() {
        SSLContext sslContext = null;

        try {
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustManagers, null);
        }
        catch (Exception e) {
            Log.e("SSLException", e.getMessage());
        }

        return sslContext;
    }

    public TrustManager[] getTrustManagers() {
        return trustManagers;
    }

    public HostnameVerifier getHostnameVerifier() {
        return hostnameVerifier;
    }
}
