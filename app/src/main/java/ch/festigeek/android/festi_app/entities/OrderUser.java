package ch.festigeek.android.festi_app.entities;

public class OrderUser implements Comparable<OrderUser> {

    private final int orderId;
    private final String mUsername;
    private boolean mIsCheckedIn;

    public OrderUser(final int oid, final String username, final int used) {
        orderId = oid;
        mUsername = username;
        mIsCheckedIn = used != 0;
    }

    public int getOrderId() {
        return orderId;
    }

    public String getUsername() {
        return mUsername;
    }

    public boolean isCheckedIn() {
        return mIsCheckedIn;
    }

    public boolean contains(String s) {
        return s == null || s.isEmpty() || mUsername.toLowerCase().contains(s.toLowerCase());
    }

    public int compareTo(OrderUser su) {
        return mUsername.compareToIgnoreCase(su.getUsername());
    }
}
