package ch.festigeek.android.festi_app.entities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.LinkedList;

import ch.festigeek.android.festi_app.interfaces.IKeys;

public class Order implements IKeys {

    private final Integer orderId;
    private final Integer state;
    private final String codeLan;
    private final User user;
    private final Team team;
    private final String payment_type;
    private final Double total;
    private final LinkedList<Product> products = new LinkedList<>();
    private final String data;

    public Order(JSONObject order) throws JSONException, ParseException {
        orderId = order.getInt(KEY_ID);
        state = order.getInt(KEY_STATE);
        codeLan = order.getString(KEY_CODE_LAN);
        user = new User(order.getJSONObject(KEY_USER));
        team = new Team(order.getJSONObject(KEY_TEAM));
        payment_type = order.getJSONObject(KEY_PAYMENT_TYPE).getString(KEY_PAYMENT_TYPE_NAME);
        total = order.getDouble(KEY_TOTAL);
        data = order.toString();

        JSONArray productsList = order.getJSONArray(KEY_PRODUCTS);
        for(int i=0; i<productsList.length(); i++)
            products.add(new Product(productsList.getJSONObject(i)));
    }

    public Integer getOrderId() {
        return orderId;
    }

    public Integer getState() {
        return state;
    }

    public String getCodeLan() {
        return codeLan;
    }

    public Team getTeam() {
        return team;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public Double getTotal() {
        return total;
    }

    public String getFormattedTotal(String pattern) {
        DecimalFormat format = new DecimalFormat(pattern);
        format.setDecimalSeparatorAlwaysShown(true);
        format.setMinimumFractionDigits(2);

        return format.format(total);
    }

    public User getUser() {
        return user;
    }

    public LinkedList<Product> getProducts() {
        return products;
    }

    @Override
    public String toString() {
        return super.toString() + " => " + data;
    }

    // SPECIAL

    public Boolean isCheckedIn() {
        for(int i=0; i<products.size(); i++) {
            Product p = products.get(i);
            if (p.isTournament())
                return p.getAmount().equals(p.getConsume());
        }
        return false;
    }

    public Boolean hasPaid() {
        return state == 1;
    }
}
