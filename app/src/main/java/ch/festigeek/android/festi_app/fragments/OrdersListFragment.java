package ch.festigeek.android.festi_app.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.ListViewCompat;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ch.festigeek.android.festi_app.MainActivity;
import ch.festigeek.android.festi_app.R;
import ch.festigeek.android.festi_app.adapters.ListUsersAdapter;
import ch.festigeek.android.festi_app.entities.OrderUser;
import ch.festigeek.android.festi_app.interfaces.FGFragment;
import ch.festigeek.android.festi_app.interfaces.IKeys;
import ch.festigeek.android.festi_app.interfaces.IURL;
import ch.festigeek.android.festi_app.utils.CommunicationManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SwipeRefreshLayout.OnRefreshListener} interface
 * to handle swiping down events.
 * Use the {@link OrdersListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrdersListFragment extends Fragment implements FGFragment, IKeys, IURL, SwipeRefreshLayout.OnRefreshListener {
    private OnFragmentInteractionListener mListener;

    private final String FRAGMENT_TAG = "USER_ORDERS_FRAGMENT";
    private final String CURRENT_EVENT = "2";
    private final String ORDERS_REQUEST = "ORDERS";

    private MainActivity activity;

    // Data
    private RequestQueue queue = null;
    private SharedPreferences sharedPref = null;
    private ListUsersAdapter mAdapter;

    private Unbinder unbinder;
    @BindView(R.id.ordersSwipeRefresh)
    protected SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.listView)
    protected ListViewCompat listView;
    protected SearchView searchView;

    public OrdersListFragment() {
        // Required empty public constructor
    }

    public static OrdersListFragment newInstance() {
        OrdersListFragment fragment = new OrdersListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_orders, container, false);
        setHasOptionsMenu(true);
        activity = (MainActivity) getActivity();

        unbinder = ButterKnife.bind(this, view);
        sharedPref = getActivity().getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);
        swipeRefreshLayout.setOnRefreshListener(this);

        mAdapter = new ListUsersAdapter(getActivity(), R.layout.adapter_order, activity.getCommandsList());

        HurlStack hurlStack = activity.communicationManager.createHurlStack(getActivity());
        queue = Volley.newRequestQueue(getContext(), hurlStack);

        listView.setAdapter(mAdapter);
        listView.setEmptyView(getActivity().findViewById(R.id.placeholder));
        // Active swipeRefreshLayout only if on top of linearLayout
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Integer topRowVerticalPosition = (listView == null || listView.getChildCount() == 0) ? 0 : listView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });

        if(activity.getCommandsList().isEmpty())
            attemptGetUserInfo();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.actions_search_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        // Associate searchable configuration with the SearchView
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem mSearchMenuItem = menu.findItem(R.id.action_search);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void attemptGetUserInfo() {
        activity.showProgressBar();

        Log.i("REQUEST", ORDERS_BY_EVENT + CURRENT_EVENT);
        JsonArrayRequest jsObjRequest = new JsonArrayRequest(Request.Method.GET, ORDERS_BY_EVENT + CURRENT_EVENT, null,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    processOrdersList(response);
                    activity.hideProgressBar();
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CommunicationManager.errorMessages(error, getActivity());
                    activity.hideProgressBar();
                }
            }) {  // needs headers for the request, override header function of this object (not the general class)
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", "Bearer " + sharedPref.getString(getString(R.string.sp_token), ""));
                return headers;
            }
        };

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                CommunicationManager.MAX_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        jsObjRequest.setTag(ORDERS_REQUEST);
        queue.add(jsObjRequest);
    }

    private void processOrdersList(JSONArray ordersArray) {
        activity.getCommandsList().clear();
        try {
            for (int i = 0; i < ordersArray.length(); ++i) {
                JSONObject JSONOrder = ordersArray.getJSONObject(i);
                JSONObject JSONUser = JSONOrder.getJSONObject(KEY_USER);
                JSONArray products = JSONOrder.getJSONArray(KEY_PRODUCTS);

                Integer oid = JSONOrder.getInt(KEY_ID);
                String username = JSONUser.getString(KEY_USERNAME);
                Integer used = 0;
                for (int j = 0; j < products.length(); ++j) {
                    JSONObject JSONProduct = products.getJSONObject(j);
                    if (JSONProduct.getInt(KEY_PRODUCT_TYPE_ID) == 1) {
                        used = JSONProduct.getJSONObject(KEY_PIVOT).getInt(KEY_CONSUME);
                        break;
                    }
                }
                OrderUser oUser = new OrderUser(oid, username, used);
                activity.getCommandsList().add(oUser);
            }
            Collections.sort(activity.getCommandsList());
        } catch (JSONException ex) {
            Log.e("LIST USERS ERROR", ex.getMessage());
        }
        finally {
            activity.hideProgressBar();
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public Integer getFragmentTitle() {
        return R.string.fragment_users_list_title;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
        else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        attemptGetUserInfo();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        mListener = null;
        queue.cancelAll(ORDERS_REQUEST);
        unbinder.unbind();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
