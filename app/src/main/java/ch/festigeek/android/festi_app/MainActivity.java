package ch.festigeek.android.festi_app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.mimic.oauth2library.OAuthResponse;
import ca.mimic.oauth2library.OAuthResponseCallback;
import ch.festigeek.android.festi_app.entities.OrderUser;
import ch.festigeek.android.festi_app.fragments.AccountFragment;
import ch.festigeek.android.festi_app.fragments.OrderFragment;
import ch.festigeek.android.festi_app.fragments.SettingsFragment;
import ch.festigeek.android.festi_app.fragments.OrdersListFragment;
import ch.festigeek.android.festi_app.interfaces.FGFragment;
import ch.festigeek.android.festi_app.interfaces.IKeys;
import ch.festigeek.android.festi_app.interfaces.IURL;
import ch.festigeek.android.festi_app.utils.CommunicationManager;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        AccountFragment.OnFragmentInteractionListener, OrdersListFragment.OnFragmentInteractionListener, OrderFragment.OnFragmentInteractionListener, SettingsFragment.OnFragmentInteractionListener, IKeys, IURL {

    private final String ACTIVITY_TAG = "MAIN_ACTIVITY";
    private final String FRAG_MANAGER_TAG = "FRAGMENT_INFO";
    private final Integer LOGIN_REQUEST = 100;
    private final Integer LOGIN_RESULT = 101;

    // Communication
    public CommunicationManager communicationManager = null;

    // Data
    private Boolean reload = false;
    private SharedPreferences sharedPref = null;
    private List<OrderUser> commandsList;

    // UI references.
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    protected DrawerLayout drawer;
    @BindView(R.id.nav_view)
    protected NavigationView navigationView;
    @BindView(R.id.main_progressBar)
    protected ContentLoadingProgressBar progressBar;
    protected ActionBarDrawerToggle toggle;

    // OVERRIDED METHODS

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        communicationManager = new CommunicationManager(this);
        sharedPref = getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);
        commandsList = new LinkedList<>();

        navigationView.setNavigationItemSelectedListener(this);
        setSupportActionBar(toolbar);

        // Set UI
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        // Test Token
        if(!sharedPref.contains(getString(R.string.sp_token)) || !sharedPref.contains(getString(R.string.sp_refresh_token))) {
            startActivityForResult(new Intent(this, LoginActivity.class), LOGIN_REQUEST);
        }
        else {
            Long expiresAt = sharedPref.getLong(getString(R.string.sp_expires_at), 0);

            if(new Date().after(new Date(expiresAt)))
                refreshToken();

            changeFragment(AccountFragment.class);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        this.onNavigationItemSelected(item);

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()){
            case R.id.nav_profile:
                changeFragment(AccountFragment.class);
                break;
            case R.id.nav_users:
                changeFragment(OrdersListFragment.class);
                break;
            case R.id.nav_scanner:
//                changeFragment(ScanFragment.class);
                break;
            case R.id.nav_preferences:
//                changeFragment(SettingsFragment.class);
                break;
            case R.id.nav_logout:
                Toast.makeText(getApplicationContext(), getString(R.string.action_signed_out), Toast.LENGTH_SHORT).show();
                logout();
                break;
            case android.R.id.home:
                onBackPressed();
            case R.id.nav_test:
                toggleProgressBar();
                break;
            default:
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOGIN_REQUEST) {
            if (resultCode == LOGIN_RESULT) {
                String resultLogin = data.getStringExtra("login");
                Log.i("LOGIN_STATE", resultLogin);

                if(resultLogin != null && resultLogin.equals("failed"))
                    finish();
            }
        }

        if(!sharedPref.contains(getString(R.string.sp_token)))
            finish();

        changeFragment(AccountFragment.class, null,false, false);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    // SPECIFIC METHODS

    // GETTER / SETTER

    public ContentLoadingProgressBar getProgressBar() {
        return progressBar;
    }

    public List<OrderUser> getCommandsList() {
        return commandsList;
    }

    // OTHERS

    public void toggleProgressBar() {
        if(progressBar.isShown())
            hideProgressBar();
        else
            showProgressBar();
    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void refreshToken() {
        communicationManager.getClient().refreshAccessToken(sharedPref.getString(getString(R.string.sp_refresh_token), ""), new OAuthResponseCallback() {
            @Override
            public void onResponse(final OAuthResponse response) {
                if (response != null && response.isSuccessful()) {
                    Log.i(ACTIVITY_TAG, "TOKEN REFRESHED !");

                    SharedPreferences.Editor editor = sharedPref.edit();
                    String accessToken = response.getAccessToken();
                    String refreshToken = response.getRefreshToken();
                    Long expiresAt = response.getExpiresAt();

                    if (expiresAt != null) {
                        editor.putLong(getString(R.string.sp_expires_at), expiresAt);
                        editor.commit();
                    }

                    if (refreshToken != null) {
                        editor.putString(getString(R.string.sp_refresh_token), refreshToken);
                        editor.commit();
                    }

                    if (accessToken != null) {
                        editor.putString(getString(R.string.sp_token), accessToken);
                        editor.commit();
                    }
                }
                else logout();
            }
        });
    }

    public void logout() {
        sharedPref.edit().remove(getString(R.string.sp_expires_at)).commit();
        sharedPref.edit().remove(getString(R.string.sp_token)).commit();
        sharedPref.edit().remove(getString(R.string.sp_refresh_token)).commit();
        startActivityForResult(new Intent(this, LoginActivity.class), LOGIN_REQUEST);
    }

    /**
     * Change the current displayed fragment by a new one.
     * - if the fragment is in backstack, it will pop it
     * - if the fragment is already displayed (trying to change the fragment with the same), it will not do anything
     *
     * @param fragmentClass   the class of the new fragment to display
     * @param bundle          if we want to send data to the new fragment
     * @param addToBackStack  if we want the fragment to be in backstack
     * @param animate         if we want a nice animation or not
     */
    public void changeFragment(Class fragmentClass, Bundle bundle, Boolean addToBackStack, Boolean animate) {
        Fragment frag = null;

        try {
            frag = (Fragment) fragmentClass.newInstance();
            if(bundle != null)
                frag.setArguments(bundle);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        String backStateName = ((Object) frag).getClass().getName();

        try {
            FragmentManager manager = getSupportFragmentManager();
            boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

            //don't add a fragment of the same type on top of itself.
            Fragment currentFragment = manager.findFragmentByTag(backStateName);
            if (currentFragment != null && currentFragment.isVisible()) {
                if (currentFragment.getClass() == fragmentClass) {
                    Log.w("Fragment Manager", "Tried to add a fragment of the same type to the backstack. This may be done on purpose in some circumstances but generally should be avoided.");
                    return;
                }
            }

            if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) {
                //fragment not in back stack, create it.
                FragmentTransaction transaction = manager.beginTransaction();

                if (animate) {
                    Log.i(FRAG_MANAGER_TAG, "Change Fragment: animate");
                    transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                }

                transaction.replace(R.id.content, frag, backStateName);

                if (addToBackStack) {
                    Log.i(FRAG_MANAGER_TAG, "Change Fragment: addToBackTack " + backStateName);
                    transaction.addToBackStack(backStateName);
                }

                setToolbarTitle((FGFragment) frag);
                transaction.commit();

                // Wait for the end of the commit then update UI
                manager.executePendingTransactions();
                setBackDrawerButton(manager);
            }
        } catch (IllegalStateException exception) {
            Log.w(FRAG_MANAGER_TAG, "Unable to commit fragment, could be activity as been killed in background. " + exception.toString());
        }
    }

    public void changeFragment(Class fragmentClass, Bundle bundle, boolean addToBackStack) {
        changeFragment(fragmentClass, bundle, addToBackStack, true);
    }

    public void changeFragment(Class fragmentClass, Bundle bundle) {
        changeFragment(fragmentClass, bundle, false, true);
    }

    public void changeFragment(Class fragmentClass) {
        changeFragment(fragmentClass, null, false, true);
    }

    public Boolean doReloadData() {
        reload = (reload) ? false : reload;
        return reload;
    }

    public void setReloadData(Boolean change) {
        reload = change;
    }

    public void setBackDrawerButton(final FragmentManager fragmentManager) {
        final ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            if(fragmentManager.getBackStackEntryCount() > 0) {
                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                toggle.setDrawerIndicatorEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(true);
                toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fragmentManager.popBackStackImmediate();
                        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

                        actionBar.setDisplayHomeAsUpEnabled(false);
                        toggle.setDrawerIndicatorEnabled(true);
                        toggle.setToolbarNavigationClickListener(null);

                        FGFragment fgFrag = (FGFragment) fragmentManager.findFragmentById(R.id.content);
                        if(fgFrag != null)
                           setToolbarTitle(fgFrag);
                    }
                });
            }
        }
    }

//    USING FGFragment informations to change the UI

    public void setToolbarTitle(FGFragment f) {
        if(f != null && getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(f.getFragmentTitle()));
        }
    }
}
