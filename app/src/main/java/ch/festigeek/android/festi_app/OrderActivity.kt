package ch.festigeek.android.festi_app

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import ch.festigeek.android.festi_app.interfaces.FGFragment

class OrderActivity : AppCompatActivity(), FGFragment {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)
    }

    override fun getFragmentTitle(): Int = R.string.fragment_order_title
}
