package ch.festigeek.android.festi_app.entities;

import android.webkit.JavascriptInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ch.festigeek.android.festi_app.interfaces.IKeys;

/**
 * Created by Pierre-Alain Curty on 02.02.2018.
 */

public class User implements IKeys {

    private final Integer userId;
    private final String email;
    private final String username;
    private final Date birthdate;
    private final String gender;
    private final String firstName;
    private final String lastName;
    private final String street;
    private final String npa;
    private final String city;
    private final String lol_account;
    private final String steamID64;
    private final String battleTag;
    private final String data;

    public User(JSONObject user) throws JSONException, ParseException {
        userId = user.getInt(KEY_ID);
        email = user.getString(KEY_EMAIL);
        username = user.getString(KEY_USERNAME);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        birthdate = sdf.parse(user.getString(KEY_BIRTHDATE));
        gender = user.getString(KEY_GENDER);
        firstName = user.getString(KEY_FIRSTNAME);
        lastName = user.getString(KEY_LASTNAME);
        street = user.getString(KEY_STREET);
        npa = user.getString(KEY_NPA);
        city = user.getString(KEY_CITY);
        lol_account = user.getString(KEY_LOL_ACCOUNT);
        steamID64 = user.getString(KEY_STEAM);
        battleTag = user.getString(KEY_BATTLE_TAG);
        data = user.toString();
    }

    public Integer getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public String getGender() {
        return gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getStreet() {
        return street;
    }

    public String getNpa() {
        return npa;
    }

    public String getCity() {
        return city;
    }

    public String getLol_account() {
        return lol_account;
    }

    public String getSteamID64() {
        return steamID64;
    }

    public String getBattleTag() {
        return battleTag;
    }

    @Override
    public String toString() {
        return super.toString() + " => " + data;
    }

    // SPECIAL

    public Boolean isUnderage() {
        Long now = new Date().getTime()/1000;
        Long age = birthdate.getTime()/1000;

        // 568025136 = 18 x seconds in year
        return (now - age) < 568025136;
    }
}
