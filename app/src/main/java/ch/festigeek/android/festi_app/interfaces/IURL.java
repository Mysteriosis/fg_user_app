package ch.festigeek.android.festi_app.interfaces;

public interface IURL {
    String URL_API = "https://api.festigeek.ch";
    String VERSION_API = "/v1";

    String OAUTH_TOKEN = URL_API + "/oauth/token";
    String OAUTH_REFRESH_TOKEN = URL_API + "/oauth/token/refresh";
    String OAUTH_DELETE_TOKEN = URL_API + "/oauth/tokens/";

    String PROFILE = URL_API + VERSION_API + "/users/me";
    String USERS = URL_API + VERSION_API + "/users/";
    String LOGIN = URL_API + VERSION_API + "/users/login";
    String QR_DECRYPT = URL_API + VERSION_API + "/qrcode/decrypt";

    String ORDERS = URL_API + VERSION_API + "/orders/";
    String ORDERS_BY_EVENT = URL_API + VERSION_API + "/orders?eventId=";
    String USER_ORDERS = URL_API + VERSION_API + "/orders?userId=";
    String CHECK_IN = URL_API + VERSION_API + "/abstract_products"; // inscription/{id} PATCH
    String PRODUCTS = URL_API + VERSION_API + "/products/";
    String CONSUME = URL_API + VERSION_API + "/productConsumption";
}
