package ch.festigeek.android.festi_app.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.concurrent.Callable;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.X509TrustManager;

import ca.mimic.oauth2library.OAuth2Client;
import ch.festigeek.android.festi_app.BuildConfig;
import ch.festigeek.android.festi_app.MainActivity;
import ch.festigeek.android.festi_app.interfaces.IURL;
import okhttp3.OkHttpClient;

/**
 * Created by Pierre-Alain Curty on 08.05.2018.
 */
public class CommunicationManager implements IURL {
    private static CommunicationManager instance = null;

    public final static Integer MAX_TIMEOUT = 10000; // 10 sec. http timeout

    private SSLExceptions sslExceptionsInstance = null;
    private OkHttpClient okHttpClient = null;

    public static void errorMessages(VolleyError error, Context context) {
        if(error != null) {
            if (error instanceof NetworkError) {
                Toast.makeText(context, "Network error.", Toast.LENGTH_LONG).show();
            } else if (error instanceof ServerError) {
                Toast.makeText(context, "Server error.", Toast.LENGTH_LONG).show();
            } else if (error instanceof AuthFailureError) {
                Toast.makeText(context, "Authentication error.", Toast.LENGTH_LONG).show();
            } else if (error instanceof ParseError) {
                Toast.makeText(context, "Parse error.", Toast.LENGTH_LONG).show();
            } else if (error instanceof TimeoutError) {
                Toast.makeText(context, "Timeout error.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, "Undefined error.", Toast.LENGTH_LONG).show();
            }
        }
    }

    public CommunicationManager(Context context) {
        sslExceptionsInstance = SSLExceptions.getInstance();

        if(sslExceptionsInstance.getTrustManagers() == null) {
            try {
                sslExceptionsInstance.addCertificate(context.getAssets().open("festigeek.ch.crt"), "festigeek.ch");
            } catch (IOException ioe) {
                Log.e("ERREUR", "Can't load SSL public key");
            }
        }

        okHttpClient = new OkHttpClient.Builder()
            .sslSocketFactory(sslExceptionsInstance.getSslContext().getSocketFactory(), (X509TrustManager) sslExceptionsInstance.getTrustManagers()[0])
            .build();
    }

    public OAuth2Client getClient() {
        return new OAuth2Client.Builder(BuildConfig.FestiGeekAPIId, BuildConfig.FestiGeekAPISecret, OAUTH_TOKEN)
            .okHttpClient(okHttpClient)
            .build();
    }

    public OAuth2Client getClient(String email, String password) {
        return this.getClient(email, password, "*");
    }

    public OAuth2Client getClient(String email, String password, String scope) {
        return new OAuth2Client.Builder(BuildConfig.FestiGeekAPIId, BuildConfig.FestiGeekAPISecret, OAUTH_TOKEN)
            .scope(scope)
            .username(email)
            .password(password)
            .okHttpClient(okHttpClient)
            .build();
    }

    public HurlStack createHurlStack(Context context) {
        return new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
                try {
                    httpsURLConnection.setSSLSocketFactory(sslExceptionsInstance.getSslContext().getSocketFactory());
//                    httpsURLConnection.setHostnameVerifier(sslExceptionsInstance.getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };
    }
}