package ch.festigeek.android.festi_app.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.ListViewCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ch.festigeek.android.festi_app.MainActivity;
import ch.festigeek.android.festi_app.R;
import ch.festigeek.android.festi_app.adapters.ListProductAdapter;
import ch.festigeek.android.festi_app.entities.Order;
import ch.festigeek.android.festi_app.interfaces.FGFragment;
import ch.festigeek.android.festi_app.interfaces.IKeys;
import ch.festigeek.android.festi_app.interfaces.IURL;
import ch.festigeek.android.festi_app.utils.CommunicationManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrderFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrderFragment extends Fragment implements FGFragment, IKeys, IURL {
    private OnFragmentInteractionListener mListener;

    private final String FRAGMENT_TAG = "ORDER_FRAGMENT";
    private final String ORDER_REQUEST = "ORDER";

    private MainActivity activity;

    // Data
    private RequestQueue queue = null;
    private SharedPreferences sharedPref = null;

    private ListProductAdapter productsAdapter;
    private LinkedList products;

    private Unbinder unbinder;
    @BindView(R.id.is_checked_in)
    protected AppCompatCheckBox mCheckedIn;
    @BindView(R.id.parental_authorization)
    protected AppCompatTextView mParentalAuthorization;
    @BindView(R.id.check_payment)
    protected AppCompatTextView mHasPaid;
    @BindView(R.id.user_name)
    protected AppCompatTextView mUsername;
    @BindView(R.id.order_infos)
    protected AppCompatTextView mInfos;
    @BindView(R.id.listProducts)
    protected ListViewCompat listViewCompat;

    public OrderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment OrderFragment.
     */
    public static OrderFragment newInstance() {
        return new OrderFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_order, container, false);
        activity = (MainActivity) getActivity();

        unbinder = ButterKnife.bind(this, view);
        sharedPref = getActivity().getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);

        products = new LinkedList<>();
        productsAdapter = new ListProductAdapter(getActivity(), R.layout.adapter_product, products);
        listViewCompat.setAdapter(productsAdapter);

        HurlStack hurlStack = activity.communicationManager.createHurlStack(getActivity());
        queue = Volley.newRequestQueue(getContext(), hurlStack);

        mCheckedIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCheckedIn.isChecked()) {
                    mCheckedIn.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.lightGreen));
                    checkIn(1);
                } else {
                    mCheckedIn.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.lightRed));
                    checkIn(0);
                }
            }
        });

        attemptGetOrder(getArguments().getInt(KEY_ORDER_ID));

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void attemptGetOrder(Integer orderId) {
        activity.showProgressBar();

        Log.i("REQUEST", ORDERS + orderId);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, ORDERS + orderId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            processOrder(new Order(response));
                        }
                        catch (Exception e) {
                            Toast.makeText(getContext(), getString(R.string.error_unprocessable_object), Toast.LENGTH_SHORT).show();
                            Log.e(FRAGMENT_TAG, e.getMessage());
                        }
                        finally {
                            activity.hideProgressBar();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        CommunicationManager.errorMessages(error, getActivity());
                        activity.hideProgressBar();
                    }
                }) { // needs headers for the request, override header function of this object (not the general class)
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", "Bearer " + sharedPref.getString(getString(R.string.sp_token), ""));
                return headers;
            }
        };

        jsObjRequest.setTag(ORDER_REQUEST);
        queue.add(jsObjRequest);
    }

    public void processOrder(Order order) {
        if (order.isCheckedIn()) {
            mCheckedIn.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.lightGreen));
            mCheckedIn.setChecked(true);
        }

        if (order.getUser().isUnderage()) {
            mParentalAuthorization.setText(getString(R.string.do_need_parental_authorization));
            mParentalAuthorization.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.lightRed));
        }

        if (order.hasPaid()) {
            mHasPaid.setText(getString(R.string.has_pay));
            mHasPaid.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.lightGreen));
        }

        String username = order.getUser().getUsername();
        String infos = getString(R.string.text_infos,
            order.getTeam().getName(),
            order.getTeam().getNumberOfMember(),
            order.getCodeLan(),
            order.getPayment_type(),
            order.getFormattedTotal("#.##"));

        mUsername.setText(username);
        mInfos.setText(Html.fromHtml(infos, Html.FROM_HTML_MODE_COMPACT));

        productsAdapter.clear();
        productsAdapter.addAll(order.getProducts());
        productsAdapter.notifyDataSetChanged();
    }

    private void checkIn(int value) {
//        Map<String, Integer> map = mUser.getInscriptionMap();
//        map.put(KEY_USED, value);
//        String url = BASE_URL + ORDERS + mUser.getOrderId() + PRODUCTS + mUser.getProductId();
//        new RequestPATCH(new ICallback<String>() {
//            @Override
//            public void success(String result) {
//                Utilities.dismissProgressDialog();
//                Log.e(LOG_NAME, result);
//            }
//
//            @Override
//            public void failure(Exception ex) {
//                Log.e(LOG_NAME, ex.getMessage());
//                mCheckedIn.setChecked(false);
//                mCheckedIn.setBackgroundColor(getResources().getColor(R.color.softRed));
//                Utilities.dismissProgressDialog();
//            }
//        }, Utilities.getFromSharedPreferences(this, "token"), url, "consume", value).execute();
    }

    @Override
    public Integer getFragmentTitle() {
        return R.string.fragment_order_title;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        mListener = null;
        queue.cancelAll(ORDER_REQUEST);
        unbinder.unbind();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}